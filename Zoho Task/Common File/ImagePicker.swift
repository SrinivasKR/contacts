//
//  ImagePicker.swift
//  Zoho Task
//
//  Created by CSS on 14/11/21.
//

import Foundation
import UIKit


fileprivate var imageCompletion : ((UIImage?)->())?

extension UIViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    func showImage(with completion : @escaping ((UIImage?)->())){
        
        let alert = UIAlertController(title: "Select Source", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: { (Void) in
            self.chooseImage(with: .photoLibrary)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:nil))
        alert.view.tintColor = .blue
        imageCompletion = completion
        self.present(alert, animated: true, completion: nil)
        
    }
    
    
    
    
    private func chooseImage(with source : UIImagePickerController.SourceType){
        
        if UIImagePickerController.isSourceTypeAvailable(source) {
            
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = source
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.present(imagePicker, animated: true, completion: nil)
        }
        
    }
    
    
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true) {
            if let image = info[UIImagePickerController.InfoKey.originalImage]  as? UIImage {
                imageCompletion?(image)
            }
        }
    }
    
    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
}
