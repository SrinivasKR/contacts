//
//  MyCustomCell.swift
//  Zoho Task
//
//  Created by CSS on 13/11/21.
//


import UIKit

class MyCustomCell: UITableViewCell {
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var companyLbl: UILabel!
    
}
