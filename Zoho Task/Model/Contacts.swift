//
//  Contacts.swift
//  Zoho Task
//
//  Created by CSS on 13/11/21.
//

import Foundation
struct ContactEntity : Codable {
    let name : String?
    let phone : String?
    let email : String?
    let address : String?
    let zip : String?
    let country : String?
    let id : Int?
    let company : String?
    let photo : String?
    let age : Int?

    enum CodingKeys: String, CodingKey {

        case name = "name"
        case phone = "phone"
        case email = "email"
        case address = "address"
        case zip = "zip"
        case country = "country"
        case id = "id"
        case company = "company"
        case photo = "photo"
        case age = "age"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        phone = try values.decodeIfPresent(String.self, forKey: .phone)
        email = try values.decodeIfPresent(String.self, forKey: .email)
        address = try values.decodeIfPresent(String.self, forKey: .address)
        zip = try values.decodeIfPresent(String.self, forKey: .zip)
        country = try values.decodeIfPresent(String.self, forKey: .country)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        company = try values.decodeIfPresent(String.self, forKey: .company)
        photo = try values.decodeIfPresent(String.self, forKey: .photo)
        age = try values.decodeIfPresent(Int.self, forKey: .age)
    }

}
