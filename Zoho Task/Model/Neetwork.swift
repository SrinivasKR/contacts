//
//  Neetwork.swift
//  Zoho Task
//
//  Created by CSS on 13/11/21.
//

import Foundation


import Foundation
import UIKit

class APIService: NSObject {
    
   
    lazy var endPoint: String = {
        return "https://shielded-ridge-19050.herokuapp.com/api?offset=\(Offset.shared.offsetValue)"
    }()
    func sendRequest( completion: @escaping ([ContactEntity]?, Error?) -> Void) {
        print("====",endPoint)
        var components = URL(string: endPoint)!
       
        let request1: NSMutableURLRequest = NSMutableURLRequest(url: components)
        
        request1.httpMethod = "GET"
        let queue:OperationQueue = OperationQueue()
        
        NSURLConnection.sendAsynchronousRequest(request1 as URLRequest, queue: queue, completionHandler:{ (response: URLResponse?, data: Data?, error: Error?) -> Void in
            
            do{
                let jsonDecoder = JSONDecoder()
                let responseModel = try jsonDecoder.decode([ContactEntity].self, from: data!)
                completion(responseModel, nil)
            } catch let error as NSError {
                completion(nil, error)
                print(error.localizedDescription)
            }
            
            
            
            
        })
    }
    
}

enum Result<T> {
    case Success(T)
    case Error(String)
}





