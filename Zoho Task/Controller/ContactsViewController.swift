//
//  ViewController.swift
//  Zoho Task
//
//  Created by CSS on 13/11/21.
//

import UIKit
import CoreData


class ContactsViewController: UIViewController {
    
    @IBOutlet weak var contactTableView: UITableView!
    let cellReuseIdentifier = "contactCell"
    let context = ((UIApplication.shared.delegate)as! AppDelegate ) .persistentContainer.viewContext
    var contactList = [Contact]()
    var ReorderList = [Contact]()
    var contactArray = [ContactEntity]()
    private lazy var loader : UIView = {
        return createActivityIndicator(UIApplication.shared.windows.filter {$0.isKeyWindow}.first!)
    }()
    var isFirstShown = true
    var isEditingTable = false
    let activityIndicator = UIActivityIndicatorView()
    override func viewDidLoad() {
        super.viewDidLoad()
        contactTableView.delegate = self
        contactTableView.dataSource = self
       

        self.title = "Contact Manager"
        // Do any additional setup after loading the view.
//        loader.isHidden = false
        Reachability.isConnectedToNetwork { (isConnected) in
            if isConnected {
                
                let alert = UIAlertController(title: "Alert", message: "Do you want to retrieve data from cloud", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                    self.deleteAllData("Contact")
                    self.CallContactAPI()
                    self.activityIndicator.frame(forAlignmentRect: CGRect(x: 0, y: 0, width: 20, height: 20))

                    let barButton = UIBarButtonItem(customView: self.activityIndicator)
                    self.navigationItem.setRightBarButton(barButton, animated: true)
                    self.activityIndicator.startAnimating()
                   
                }))
                alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { action in
                    self.Fetchdata()
                    
                }))
                self.present(alert, animated: true, completion: nil)
              
                
            } else {
                Fetchdata()
            }
        }
       
        
    }
    
    
    
    func CallContactAPI()  {
        let service = APIService()
        service.sendRequest { (response, error) in
            if error == nil{
                self.contactArray = response ?? [ContactEntity]()
                self.savedata()
                
            } else {
                if ((Offset.shared.offsetValue%10) == 0){
                Offset.shared.DecreaseOffsetByOne()
                self.CallContactAPI()
                    DispatchQueue.main.async {
                        self.contactTableView.dragInteractionEnabled = true
                        self.contactTableView.dragDelegate = self
                        self.contactTableView.dropDelegate = self
                    }
                   
                }
                DispatchQueue.main.async {
                    self.activityIndicator.stopAnimating()
                    self.contactTableView.reloadData()
                    
                }
            }
        }
        
    }
    
    
    func savedata(){
        for index in 0..<contactArray.count{
            let Person = Contact(context: context)
            Person.name = contactArray[index].name
            Person.phone = contactArray[index].phone
            Person.email = contactArray[index].email
            Person.address = contactArray[index].address
            Person.zip = contactArray[index].zip
            Person.country = contactArray[index].country
            Person.id = Int16(contactArray[index].id!)
            Person.company = contactArray[index].company
            Person.photo = contactArray[index].photo
            Person.age = Int16(contactArray[index].age!)
            let imageURL = NSURL(string: contactArray[index].photo ?? "")
            let imagedData = NSData(contentsOf: imageURL! as URL)!
            Person.imagedata = imagedData as Data
            self.contactList.append(Person)
        }
        for i in 0..<contactList.count{
            self.contactList[i].displayorder = Int16(i)
        }
        
        do {
            try context.save()
        } catch {
            // Catch any other errors
            print("error Saving array \(error)")
        }
        
        
        loaddata()
    }
    
    
    
    func loaddata() {
        let request: NSFetchRequest<Contact> = Contact.fetchRequest()
        let nameSort = NSSortDescriptor(key:"displayorder", ascending:true)
        request.sortDescriptors = [nameSort]
        
        do{
            contactList =  try context.fetch(request)
            
            print("Fetched Data",contactList)
            DispatchQueue.main.async {
               
                    self.contactTableView.reloadData()
               
            }
            Offset.shared.IncreaseOffset()
            CallContactAPI()
            
        }catch{
            print("error Fetching string \(error)")
        }
    }
    
    func Fetchdata() {
        let request: NSFetchRequest<Contact> = Contact.fetchRequest()
        let nameSort = NSSortDescriptor(key:"displayorder", ascending:true)
        request.sortDescriptors = [nameSort]
        
        do{
            contactList =  try context.fetch(request)
            
            print("Fetched Data",contactList)
            DispatchQueue.main.async {
               
                self.contactTableView.reloadData()
                self.contactTableView.dragInteractionEnabled = true
                self.contactTableView.dragDelegate = self
                self.contactTableView.dropDelegate = self
               
            }
           
            
        }catch{
            print("error Fetching string \(error)")
        }
    }
    
    
//    func ReOrderdata(){
//        deleteAllData("Contact")
//        for index in 0..<ReorderList.count{
//            let Person = Contact(context: context)
//            Person.name = ReorderList[index].name
//            Person.phone = ReorderList[index].phone
//            Person.email = ReorderList[index].email
//            Person.address = ReorderList[index].address
//            Person.zip = ReorderList[index].zip
//            Person.country = ReorderList[index].country
//            Person.id = Int16(ReorderList[index].id)
//            Person.company = ReorderList[index].company
//            Person.photo = ReorderList[index].photo
//            Person.age = Int16(ReorderList[index].age)
//            Person.imagedata = ReorderList[index].imagedata
//            self.contactList.append(Person)
//        }
//
//        do {
//            try context.save()
//        } catch {
//            // Catch any other errors
//            print("error Saving array \(error)")
//        }
        
       
        
//    }
    
    
    func LoadTableData() {
//        let request: NSFetchRequest<Contact> = Contact.fetchRequest()
//        do{
//            contactList =  try context.fetch(request)
//            print("Fetched Data",contactList)
            DispatchQueue.main.async {
               
                self.contactTableView.reloadData()
               
            }
           
            
//        }catch{
//            print("error Fetching string \(error)")
//        }
    }
    
    func deleteAllData(_ entity:String) {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entity)
        fetchRequest.returnsObjectsAsFaults = false
        do {
            let results = try context.fetch(fetchRequest)
            for object in results {
                guard let objectData = object as? NSManagedObject else {continue}
                context.delete(objectData)
            }
        } catch let error {
            print("Detele all data in \(entity) error :", error)
        }
    }
   
    
    
    func showAlertWith(title: String, message: String, style: UIAlertController.Style = .alert) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: style)
        let action = UIAlertAction(title: title, style: .default) { (action) in
            self.dismiss(animated: true, completion: nil)
        }
        alertController.addAction(action)
        self.present(alertController, animated: true, completion: nil)
    }
    
    
}

extension ContactsViewController :UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("@@@@@@@!!!!!",contactList.count)
        return contactList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:MyCustomCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! MyCustomCell
        cell.profileImageView.layer.cornerRadius = cell.profileImageView.frame.width/2
        cell.nameLbl.text = contactList[indexPath.row].name
        cell.companyLbl.text = contactList[indexPath.row].company
        cell.profileImageView?.image = UIImage(data: contactList[indexPath.row].imagedata ?? Data())
     
        return cell
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let DetailVC = self.storyboard?.instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
        DetailVC.contactDetails = contactList[indexPath.row]
        DetailVC.contactList = contactList
        DetailVC.Contactindex = indexPath.row
        DetailVC.ChangeImageData = contactList[indexPath.row].imagedata!
        DetailVC.fetchData = {
            self.LoadTableData()
        }
        self.navigationController?.pushViewController(DetailVC, animated: true)
    }
    
    
}
extension ContactsViewController: UITableViewDragDelegate {
func tableView(_ tableView: UITableView, itemsForBeginning session: UIDragSession, at indexPath: IndexPath) -> [UIDragItem] {
   
        return [UIDragItem(itemProvider: NSItemProvider())]
    }

    
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        contactList[sourceIndexPath.row].displayorder = Int16(destinationIndexPath.row)
        contactList[destinationIndexPath.row].displayorder = Int16(sourceIndexPath.row)

            do{
                try context.save()
                Fetchdata()
            }catch{
                print("Rows could not be saved")
            }
        
    }

}

extension ContactsViewController: UITableViewDropDelegate {
    func tableView(_ tableView: UITableView, dropSessionDidUpdate session: UIDropSession, withDestinationIndexPath destinationIndexPath: IndexPath?) -> UITableViewDropProposal {

        if session.localDragSession != nil { // Drag originated from the same app.
            return UITableViewDropProposal(operation: .move, intent: .insertAtDestinationIndexPath)
        }

        return UITableViewDropProposal(operation: .cancel, intent: .unspecified)
    }

    func tableView(_ tableView: UITableView, performDropWith coordinator: UITableViewDropCoordinator) {
    }
}


extension UIImageView {
    public func imageFromUrl(urlString: String) {
        if let url = NSURL(string: urlString) {
            let request = NSURLRequest(url: url as URL)
            NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {
                (response: URLResponse?, data: Data?, error: Error?) -> Void in
                if let imageData = data as NSData? {
                    self.image = UIImage(data: imageData as Data)
                }
            }
        }
    }
}
