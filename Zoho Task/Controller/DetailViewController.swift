//
//  DetailViewController.swift
//  Zoho Task
//
//  Created by CSS on 14/11/21.
//

import UIKit
import MessageUI

class DetailViewController: UIViewController {

    
    
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var designationLbl: UILabel!
    @IBOutlet weak var companyLbl: UILabel!
    @IBOutlet weak var phoneTF: UITextField!
    @IBOutlet weak var AltPhoneTF: UITextField!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var websiteTF: UITextField!
    @IBOutlet weak var addressTF: UITextField!
    @IBOutlet weak var scrollView: UIScrollView!
    
    var contactDetails = Contact()
    var contactList = [Contact]()
    var Contactindex = Int()
    var ChangeImageData = Data()
    let context = ((UIApplication.shared.delegate)as! AppDelegate ) .persistentContainer.viewContext
    var fetchData:(() -> Void)?
   var Mail = ""
    var dummyBtn = UIBarButtonItem()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.title = "Details"
        
        print("Index@@@@",Contactindex)
        profileImageView.layer.cornerRadius = profileImageView.frame.width/2
        [phoneTF,AltPhoneTF,emailTF,websiteTF,addressTF].forEach { (Field) in
            Field?.addBottomBorder()
            Field?.isUserInteractionEnabled = false
        }
        
        
        let rightButtonItem = UIBarButtonItem.init(
              title: "Edit",
            style: .done,
              target: self,
            action: #selector(rightButtonAction)
        )
        self.navigationItem.rightBarButtonItem = rightButtonItem
        self.navigationItem.rightBarButtonItem?.tintColor = .red
        
        
        
        let leftButtonItem = UIBarButtonItem.init(
              title: "Back",
            style: .done,
              target: self,
            action: #selector(leftButtonAction)
        )
        self.navigationItem.leftBarButtonItem = leftButtonItem
        
        self.navigationItem.leftBarButtonItem?.tintColor = .red
        
        
        
        self.profileImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action:  #selector(self.handleTap(_:))))
        self.profileImageView.isUserInteractionEnabled = true

        setData()
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardDidShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardDidHideNotification, object: nil)

        self.hideKeyboardWhenTappedAround()

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 300, right: 0)

    }
    
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            print(keyboardSize.height)
            
            scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardSize.height, right: 0)

    }}
    
    @objc func keyboardWillHide(notification: NSNotification) {
      
            scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)

     }
    func setData(){
        profileImageView.image =  UIImage(data: contactDetails.imagedata ?? Data())
        nameLbl.text = contactDetails.name
        designationLbl.text = "Employee"
        companyLbl.text = contactDetails.company
        phoneTF.text = contactDetails.phone
        AltPhoneTF.text = contactDetails.phone
        emailTF.text = contactDetails.email
       
        addressTF.text = contactDetails.address
        guard let company = contactDetails.company else{
            return
        }
        let companyTrimmmed = company.filter { !$0.isWhitespace }
        websiteTF.text = "WWW.\(companyTrimmmed).com"
        Mail = contactDetails.email ?? ""
    }
    
    
    @objc func rightButtonAction(sender: UIBarButtonItem){
        let Title = sender.title ?? ""
        
        if Title == "Edit"{
            self.navigationItem.rightBarButtonItem?.title = "Save"
            [phoneTF,emailTF,addressTF].forEach { (Field) in
                Field?.isUserInteractionEnabled = true
            }
            
            
            
            
        } else {
            self.navigationItem.rightBarButtonItem?.title = "Edit"
            [phoneTF,emailTF,addressTF].forEach { (Field) in
                Field?.isUserInteractionEnabled = false
            }
            saveData()
        }
        
        
        
        
    }
    
    @objc func leftButtonAction(sender: UIBarButtonItem){
        fetchData?()
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func callButtonTapped(_ sender: Any) {
        call(to: contactDetails.phone)
    }
    
    @IBAction func mailButtonTapped(_ sender: Any) {
        self.sendEmail()
    }
    @IBAction func deleteBtntapped(_ sender: Any) {
        context.delete(contactList[Contactindex])
        do {
            leftButtonAction(sender: dummyBtn)
            try context.save()
        } catch {
            // Catch any other errors
            print("error Saving array \(error)")
        }
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil){
        
        self.showImage { (image) in
            if image != nil {
                self.profileImageView.image = image
                guard let pngdata = image?.pngData() else { return }
                self.ChangeImageData = pngdata
                self.contactList[self.Contactindex].imagedata = self.ChangeImageData
                
                do {
                    try self.context.save()
                } catch {
                    // Catch any other errors
                    print("error Saving array \(error)")
                }
                
            }
        }
    }
    
    func saveData(){
  
        contactList[Contactindex].phone = phoneTF.text ?? ""
        contactList[Contactindex].email = emailTF.text ?? ""
        contactList[Contactindex].address = addressTF.text ?? ""
        saveContext()
      
      
    }
   
    func saveContext(){
        do {
            try context.save()
        } catch {
            // Catch any other errors
            print("error Saving array \(error)")
        }
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension DetailViewController: MFMailComposeViewControllerDelegate {
    
    func sendEmail() {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients([Mail])
            mail.setSubject("Welcome")
            present(mail, animated: true)
        } else {
            let alert = UIAlertController(title: "Alert", message: "Kindly install and login the mail app", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
               
               
            }))
            self.present(alert, animated: true, completion: nil)

        }
    }
    
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
    
    
}
extension DetailViewController{
func call(to number : String?) {
    var phonenumber = number?.replacingOccurrences(of: "(", with: "")
    phonenumber = phonenumber?.replacingOccurrences(of: ")", with: "")
    phonenumber = phonenumber?.replacingOccurrences(of: "-", with: "")
//    phonenumber = phonenumber?.replacingOccurrences(of: " ", with: "")
    
    guard let providerNumber = phonenumber, let url = URL(string: "tel://\((providerNumber))") else {
        let alert = UIAlertController(title: "Alert", message: "incorrect Number", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
           
           
        }))
        present(alert, animated: true, completion: nil)
        return
    }
        UIApplication.shared.canOpenURL(url) 
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    
}
}
